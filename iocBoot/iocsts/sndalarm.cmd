#!../../bin/linux-x86_64/sts

## You may have to change sts to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/sts.dbd"
sts_registerRecordDeviceDriver pdbbase

## Load record instances

cd "${TOP}/iocBoot/${IOC}"

## Load record instances
dbLoadRecords("scanalarm.db","BL=PINK,DEV=SCNALM")
dbLoadRecords("alertdb.db","BL=PINK,DEV=ALERT")
dbLoadRecords("watchdog.db","BL=PINK,DEV=ALERT,COND=6")

dbLoadTemplate("snd.substitutions")
dbLoadTemplate("alert.substitutions")
dbLoadTemplate("chk.substitutions")

## autosave
set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")

iocInit

## autosave
create_monitor_set("auto_settings.req", 30, "BL=PINK,DEV=SNDAL")

## Start any sequence programs
#seq sncxxx,"user=epics"
