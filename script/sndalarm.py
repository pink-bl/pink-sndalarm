#!/usr/bin/python3
import epics
import queue
import time

## message queue to be send over SMS
global msgqueue
msgqueue = queue.Queue(20)

## function to get phone list
def getphonelist(phone, pv_error, pv_status):
    res=[]
    for pho in phone:
        try:
            if pho.check()>0:
                ph = pho.phone()
                phn = int(ph)
                if len(ph)>5:
                    res.append(ph)
        except:
            pv_error.put(1)
            pv_status.put("Error on PV:  " + pho.pvname_phone)
    return res

## function to add timestamp to message string
def addts(msg):
    tn = time.localtime()
    ts = "[{}.{}|{:02d}:{:02d}:{:02d}] {}".format(tn.tm_mday,tn.tm_mon,tn.tm_hour,tn.tm_min,tn.tm_sec,msg)
    return ts

## PV callback
def onChange(pvname=None, value=None, char_value=None, **kw):
    global msgqueue
    msg = str(value)
    if msgqueue.full()==False:
        msgqueue.put(msg)
    else:
        now = time.asctime()
        print(now + "- Message queue is full.")

class COND:
    def __init__(self,N):
        self.pvname_check = "PINK:ALERT:condition{:d}".format(int(N))
        self.pvname_state = "PINK:ALERT:cond_state{:d}".format(int(N))
        self.pvname_value = "PINK:ALERT:cond_value{:d}".format(int(N))
        self.check = epics.PV(self.pvname_check, auto_monitor=True)
        self.state = epics.PV(self.pvname_state, auto_monitor=True)
        self.val = epics.PV(self.pvname_value, auto_monitor=True)

class PHONE:
    def __init__(self,N):
        self.pvname_check = "PINK:ALERT:phonecheck{:d}".format(int(N))
        self.pvname_phone = "PINK:ALERT:phone{:d}".format(int(N))
    def check(self):
        return epics.caget(self.pvname_check)
    def phone(self):
        return epics.caget(self.pvname_phone)

## create PV channels - essential
print("Connecting essential PVs...")
pv_enable = epics.PV("PINK:ALERT:enable", auto_monitor=True)
pv_message = epics.PV("PINK:ALERT:message", auto_monitor=True, callback=onChange)
pv_sms_num = epics.PV("PINK:GSM:smsnumber")
pv_sms_msg = epics.PV("PINK:GSM:smsmsg")
pv_sms_send = epics.PV("PINK:GSM:sendsms.PROC")
pv_error = epics.PV("PINK:ALERT:error")
pv_status = epics.PV("PINK:ALERT:status")
pv_alertlog = epics.PV("PINK:ALERT:alert_log", auto_monitor=False)

## Watchdog PVs
pv_wd_state = epics.PV("PINK:ALERT:WD:state", auto_monitor=True)

## create PV channels - conditions
print("Connecting condition PVs...")
num_conditions = 15
condition=[]
for i in range(num_conditions):
    condition.append(COND(int(i+1)))

## create PV channels - phones
print("Connecting phone PVs...")
num_phones = 10
phone=[]
for i in range(num_phones):
    phone.append(PHONE(int(i+1)))

## create PV channels - monitor
print("Connecting extra PVs...")
pv_current = epics.PV("CUMZR:rdCur", auto_monitor=True)
pv_shutters = epics.PV("PINK:AUX:Shutters_RBV", auto_monitor=True)
pv_valves = epics.PV("PINK:AUX:Valves_RBV", auto_monitor=True)
pv_v15 = epics.PV("PINK:SNDAL:P1:alarmstate", auto_monitor=True)
pv_g08 = epics.PV("PINK:MAXD:S3Measure", auto_monitor=True)
pv_g04 = epics.PV("PINK:MAXB:S3Measure", auto_monitor=True)
pv_g11 = epics.PV("PINK:MAXB:S2Measure", auto_monitor=True)

## wait for PV connections and empty queue
print("Sleep wait for 3 seconds...")
time.sleep(3)
msgqueue.queue.clear()

print("[{}]".format(time.asctime()))
print("PINK alert script running ...")
pv_error.put(0)
pv_status.put("Script started")

## main loop
while(True):
    if pv_enable.value>0:

        ## condition 1 - Beam current
        try:
            cond = condition[0]
            if cond.check.value>0:
                if pv_current.value<cond.val.value:
                    if int(cond.state.value)==0:
                        msg = "Beam current:{:.1f}mA".format(pv_current.value)
                        msgqueue.put(addts(msg))
                        cond.state.put(1)
                else:
                    ## reset alert
                    if int(cond.state.value)==1:
                        cond.state.put(0)
        except:
            pv_error.put(1)
            pv_status.put("Condition 1 error")

        ## condition 2 - Shutters closed
        try:
            cond = condition[1]
            if cond.check.value>0:
                if pv_shutters.value<1:
                    if int(cond.state.value)==0:
                        msg = "Shutter has closed"
                        msgqueue.put(addts(msg))
                        cond.state.put(1)
                else:
                    ## reset alert
                    if int(cond.state.value)==1:
                        cond.state.put(0)
        except:
            pv_error.put(1)
            pv_status.put("Condition 2 error")

        ## condition 3 - Vacuum valve closed
        try:
            cond = condition[2]
            if cond.check.value>0:
                if pv_valves.value<1:
                    if int(cond.state.value)==0:
                        msg = "Vacuum valve closed"
                        msgqueue.put(addts(msg))
                        cond.state.put(1)
                else:
                    ## reset alert
                    if int(cond.state.value)==1:
                        cond.state.put(0)
        except:
            pv_error.put(1)
            pv_status.put("Condition 3 error")

        ## condition 4 - V15 closed by PLC
        try:
            cond = condition[3]
            if cond.check.value>0:
                if pv_v15.value>0:
                    if int(cond.state.value)==0:
                        msg = "V15 closed.P={:.1e}mbar".format(pv_g08.value)
                        msgqueue.put(addts(msg))
                        cond.state.put(1)
                else:
                    ## reset alert
                    if int(cond.state.value)==1:
                        cond.state.put(0)
        except:
            pv_error.put(1)
            pv_status.put("Condition 4 error")

        ## condition 5 - Insulation vacuum
        try:
            cond = condition[4]
            if cond.check.value>0:
                if pv_g08.value>cond.val.value:
                    if int(cond.state.value)==0:
                        msg = "Insulation vac.P={:.1e}mbar".format(pv_g08.value)
                        msgqueue.put(addts(msg))
                        cond.state.put(1)
                else:
                    ## reset alert
                    if int(cond.state.value)==1:
                        cond.state.put(0)
        except:
            pv_error.put(1)
            pv_status.put("Condition 5 error")


        ## condition 6 - Insulation vacuum
        try:
            cond = condition[5]
            if cond.check.value>0:
                if pv_wd_state.value>0:
                    if int(cond.state.value)==0:
                        msg = "PShell not responding"
                        msgqueue.put(addts(msg))
                        cond.state.put(1)
                else:
                    ## reset alert
                    if int(cond.state.value)==1:
                        cond.state.put(0)
        except:
            pv_error.put(1)
            pv_status.put("Condition 6 error")

    ## Send SMS message from queue
    if msgqueue.qsize()>0:
       msg=msgqueue.get()
       phonelist = getphonelist(phone, pv_error, pv_status)
       now=time.asctime()
       print(now + "\nNew message: "+msg)
       print("Phone list: " + str(phonelist) + "\n")
       pv_sms_msg.put(msg)
       for phi in phonelist:
           pv_sms_num.put(phi)
           time.sleep(1)
           pv_sms_send.put(1)
           time.sleep(1)

    ## Loop sleep time
    time.sleep(3)

